package main

import (
	"fmt"
	"math/rand"
)

type die struct {
	sides, selection int
}

func newDie(sides int) die {
	return die{sides, rand.Intn(sides) + 1}
}

func (d die) display() {
	fmt.Printf("A %d sided die rolled a: %d", d.sides, d.selection)
}

package main

import (
	"flag"
	"fmt"
	"log"
	"math/rand"
	"strconv"
	"strings"
	"sync"
	"time"
)

type result struct {
	sum  int
	data map[int]*dieStat
}

func processArg(amt, die_type int, c chan die, wg *sync.WaitGroup) {
	for i := 0; i < amt; i++ {
		c <- newDie(die_type)
	}
	wg.Done()
}

func aggregate(dc <-chan die, rc chan<- result) {
	defer close(rc)

	// Create struct of aggregate data
	r := result{0, make(map[int]*dieStat)}

	for d := range dc {
		r.sum += d.selection

		// Initialize if key doesn't exist
		if _, ok := r.data[d.sides]; !ok {
			r.data[d.sides] = &dieStat{}

		}

		// Update map
		r.data[d.sides].add(d.selection)

		// val, ok := r.data[d.sides]
		// log.Println("Val: ", val)
		// log.Println("Ok: ", ok)
	}

	rc <- r
}

func main() {
	var s int64
	flag.Int64Var(&s, "s", time.Now().UnixNano(), "Set the seed for randomization")

	var verbose bool
	flag.BoolVar(&verbose, "v", false, "Verbose")

	var q bool
	flag.BoolVar(&q, "q", false, "Quiet")

	flag.Parse()

	// Error early based on flags
	if verbose && q {
		log.Fatalln("Error: Cannot be quiet (-q) and verbose (-v) simultaneously!")
	}

	rand.Seed(s)

	dice_args := flag.Args()

	dc := make(chan die)
	rc := make(chan result)
	var wg sync.WaitGroup

	go aggregate(dc, rc)

	// Iterate through the dice args
	for _, arg := range dice_args {
		INCORRECT_FORMAT := "Die entry not in correct format `xdy` where `x` is the amount and `y` is number of faces"
		MIN_SIDE_REQUIRED := "Dice must have 2 or more sides for it to be useful..."

		split := strings.Split(arg, "d")
		if len(split) != 2 {
			log.Fatal(INCORRECT_FORMAT)
		}

		amt, err_amt := strconv.Atoi(split[0])
		die_type, err_type := strconv.Atoi(split[1])
		if err_amt != nil || err_type != nil {
			log.Fatal(INCORRECT_FORMAT)
		}

		if die_type < 2 {
			log.Fatal(MIN_SIDE_REQUIRED)
		}

		wg.Add(1)
		go processArg(amt, die_type, dc, &wg)

	}

	// Clean up
	wg.Wait()
	close(dc)

	output := <-rc

	if verbose {
		for key, value := range output.data {
			fmt.Printf("= Type %d:\n", key)
			value.display()
		}
	}
	fmt.Println("Result:", output.sum)
}

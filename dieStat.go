package main

import "fmt"

type dieStat struct {
	count, sum int
	rolled     []int
}

func (ds *dieStat) add(x int) {
	(*ds).count++
	(*ds).sum += x
	(*ds).rolled = append(ds.rolled, x)
}

func (ds *dieStat) display() {
	fmt.Printf("  - Count: %d\n", ds.count)
	fmt.Printf("  - Sum: %d\n", ds.sum)
	fmt.Printf("  - Rolls: %d\n", ds.rolled)
	fmt.Println()
}
